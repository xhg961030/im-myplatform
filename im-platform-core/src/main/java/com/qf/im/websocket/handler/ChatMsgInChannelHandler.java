package com.qf.im.websocket.handler;

import com.qf.im.entity.Message;
import com.qf.im.websocket.base.BaseChannelInHandler;
import com.qf.im.websocket.utils.ChannelManager;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@ChannelHandler.Sharable
public class ChatMsgInChannelHandler extends BaseChannelInHandler {
    @Override
    protected int action() {
        return 101;
    }

    @Override
    protected void handler(ChannelHandlerContext ctx, Message message) {
        Integer to = message.getTo();
        Channel channel = ChannelManager.get(to);
        Optional.ofNullable(channel)
                .ifPresent(c ->  {
                    //将消息原封不动的转发给对方
                    c.writeAndFlush(message);
                });
    }
}
