package com.qf.im.websocket.utils;

import io.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ChannelManager {

    private static Map<Integer, Channel> channelMap = new ConcurrentHashMap<>();
    private static Map<Channel, Integer> channelMap2 = new ConcurrentHashMap<>();

    public static void put(Integer uid, Channel channel) {
        channelMap.put(uid, channel);
        channelMap2.put(channel, uid);
    }

    public static Channel get(Integer uid) {
        return channelMap.get(uid);
    }

    public static void remove(Integer uid){
        Channel channel = channelMap.remove(uid);
        channelMap2.remove(channel);
    }

    public static void remove(Channel channel){
        Integer uid = channelMap2.remove(channel);
        channelMap.remove(uid);
    }
}
