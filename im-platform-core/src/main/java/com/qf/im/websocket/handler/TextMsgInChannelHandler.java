package com.qf.im.websocket.handler;

import com.alibaba.fastjson2.JSON;
import com.qf.im.entity.Message;
import com.qf.im.websocket.utils.ChannelManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;

/**
 * 文本帧的消息处理器
 */
@Slf4j
public class TextMsgInChannelHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.debug("【Websocket Conn】有一个客户端连接了服务器...{}", ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.debug("【Websocket DisConn】有一个客户端断开了服务器的连接...{}", ctx.channel());
        ChannelManager.remove(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame textWebSocketFrame) throws Exception {
        //获取客户端传递过来的json
        String json = textWebSocketFrame.text();
        log.debug("【websocket message】接收到客户端的消息 - {} - {}", json, ctx.channel());
        //转换成实体类
        Message msg = JSON.parseObject(json, Message.class);
        //获得了Message对象，将Message对象传递给后续的ChannelHandler处理
        ctx.fireChannelRead(msg);
    }
}
