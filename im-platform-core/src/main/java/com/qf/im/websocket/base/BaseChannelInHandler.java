package com.qf.im.websocket.base;

import com.qf.im.entity.Message;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 自定义的一个入栈消息处理器的基类 - 模板类【模板模式】
 */
public abstract class BaseChannelInHandler extends SimpleChannelInboundHandler<Message> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Message message) throws Exception {
        if (message.getAction() == action()) {
            handler(ctx, message);
        } else {
            ctx.fireChannelRead(message);
        }
    }

    protected abstract int action();

    protected abstract void handler(ChannelHandlerContext ctx, Message message);
}
