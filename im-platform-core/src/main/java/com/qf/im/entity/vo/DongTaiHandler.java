package com.qf.im.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
@Data
@Accessors(chain = true)
public class DongTaiHandler {
    private Integer id;
    //用户id
    private Integer uid;
    //动态文本发布的内容
    private String dContent;
    //动态内容中包含的图片集合
    private List<String> images;
}
