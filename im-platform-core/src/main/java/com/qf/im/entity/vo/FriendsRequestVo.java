package com.qf.im.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FriendsRequestVo {

    private Integer id;
    private String info;
    private String header;
    private String nickname;
    private Integer sex;
    private Integer status;//申请状态
}
