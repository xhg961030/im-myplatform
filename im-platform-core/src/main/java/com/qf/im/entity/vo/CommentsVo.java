package com.qf.im.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class CommentsVo {
    //评论id
    private Integer id;
    //评论评论的id,默认为0；
    private Integer cid=0;
    //动态id
    private Integer did;
    //评论内容
    private String cContent;
    //评论者的id
    private Integer fid;
    //评论者的昵称
    private String nickname;
    //评论者头像
    private String header;
    //评论时间
    private Date createTime;

}
