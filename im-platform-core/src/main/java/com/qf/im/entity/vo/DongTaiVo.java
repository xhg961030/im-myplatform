package com.qf.im.entity.vo;

import com.qf.im.entity.DImages;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class DongTaiVo {
    //动态id
    private Integer id;
    //动态内容
    private String dContent;
    //动态图片资源
    private List<DImages> dImages;
    //动态创建时间
    private Date createTime;
    //发布动态人的头像
    private String header;
    //发布动态人的id
    private String uid;
    //发布动态人的昵称
    private String nickname;

}
