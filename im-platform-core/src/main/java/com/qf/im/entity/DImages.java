package com.qf.im.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.commons.domain.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 动态图片名资源表(DImages)表实体类
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@Data
@Accessors(chain = true)

public class DImages extends BaseEntity {
    //图片id
    @TableId(type = IdType.AUTO)
    private Integer id;
    //动态id
    private Integer did;
    //图片名字
    private String imgName;



}

