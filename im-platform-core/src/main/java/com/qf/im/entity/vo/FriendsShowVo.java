package com.qf.im.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FriendsShowVo {
    private Integer id;
    private String header;
    private String nickname;
    private String beizhu;
    private Integer msgNum;
}
