package com.qf.im.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Email {
    private String subject;
    private String from;
    private String to;
    private String content;
}
