package com.qf.im.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.commons.domain.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 评论表(Comments)表实体类
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@Data
@Accessors(chain = true)

public class Comments extends BaseEntity {
    //评论表主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //评论所评论的id
    private Integer cid = 0;
    //评论者id
    private Integer fid;
    //动态id
    private Integer did;
    //评论表内容
    private String cContent;



}

