package com.qf.im.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.commons.domain.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 动态表(DongTai)表实体类
 *
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
@Data
@Accessors(chain = true)

public class DongTai extends BaseEntity {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    //用户id
    private Integer uid;
    //动态文本发布的内容
    private String dContent;



}

