package com.qf.im.entity.vo;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

@Data
@Accessors(chain = true)
public class RequestHandlerVo {

    //申请记录id
    @NotNull(message = "申请id不能为空！")
    private Integer id;
    //处理状态
    @NotNull(message = "处理状态不能为空！")
    @Range(min = 1, max = 2, message = "处理状态格式不正确")
    private Integer status;
    //好友备注
    private String beizhu;
}
