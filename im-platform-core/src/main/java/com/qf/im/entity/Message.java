package com.qf.im.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class Message<T> implements Serializable {

    private Integer action;
    private Integer from;
    private Integer to;
    private Integer type; //1-文本消息 2-图片消息
    private T msg;
}
