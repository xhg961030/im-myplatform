package com.qf.im.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.commons.domain.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 好友申请表(FriendsRequest)表实体类
 *
 * @author makejava
 * @since 2023-07-28 20:07:19
 */
@Data
@Accessors(chain = true)

public class FriendsRequest extends BaseEntity {
    //主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //申请者id
    private Integer uid;
    //被申请者id
    private Integer tid;
    //好友备注
    private String beizhu;
    //申请信息
    private String info;


}

