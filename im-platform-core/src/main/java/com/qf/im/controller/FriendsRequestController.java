package com.qf.im.controller;


import com.qf.commons.domain.R;
import com.qf.im.entity.FriendsRequest;
import com.qf.im.entity.vo.FriendsRequestVo;
import com.qf.im.entity.vo.RequestHandlerVo;
import com.qf.im.service.FriendsRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 好友申请表(FriendsRequest)表控制层
 *
 * @author makejava
 * @since 2023-07-28 20:07:19
 */
@RestController
@RequestMapping("friendsRequest")
@Slf4j
public class FriendsRequestController  {
    /**
     * 服务对象
     */
    @Autowired
    private FriendsRequestService friendsRequestService;

    /**
     * 发送好友申请的方法
     * @param friendsRequest
     * @return
     */
    @RequestMapping("/sendFriendRequest")
    public R sendFriendRequest(FriendsRequest friendsRequest){
        log.debug("send friendRequest--好友申请--{}",friendsRequest);
        friendsRequestService.save(friendsRequest);
        return R.succ();
    }

    /**
     * 查询好友申请的数量
     * @return
     */

    @RequestMapping("/requestCount")
    public R requestCount(){
        int count = friendsRequestService.queryRequestCountByMe();

        return R.succ(count);
    }

    /**
     * 查询给我的好友申请
     * @return
     */
    @RequestMapping("/queryListByMe")
    public R queryListByMe(){
        List<FriendsRequestVo> friendsRequestVos = friendsRequestService.queryRequestListByMe();
        return R.succ(friendsRequestVos);
    }
    @RequestMapping("/requestHandler")
    public R requestHandler(@Validated RequestHandlerVo requestHandlerVo){
        log.debug("[request handler] 处理好友申请记录 - {}", requestHandlerVo);
        int res = friendsRequestService.requestHandler(requestHandlerVo);
        return R.succ();

    }

}

