package com.qf.im.controller;


import com.qf.commons.domain.R;
import com.qf.im.entity.Comments;
import com.qf.im.entity.vo.CommentsVo;
import com.qf.im.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 评论表(Comments)表控制层
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@RestController
@RequestMapping("comments")
public class CommentsController  {
    /**
     * 服务对象
     */
    @Autowired
    private CommentsService commentsService;

    /**
     * 添加一条评论
     * @return
     */
    @RequestMapping("addComment")
    public R addComment(Comments comments){
        int res = commentsService.addComment(comments);
        return R.succ();
    }

    /**
     * 查看评论列表
     * 入参：动态id
     * 返回：
     SELECT c.id,c.cid,c.fid, c.c_content ,u.nickname,c.create_time,u.header
     FROM comments c INNER JOIN USER u ON u.id =c.fid  AND c.did =2
     * @return
     */
    @RequestMapping("showComments")
    public R showComments(Integer did){
        //
        List<CommentsVo> list = commentsService.queryComments(did);
        return R.succ(list);
    }
}

