package com.qf.im.controller;


import com.qf.commons.domain.Codes;
import com.qf.commons.domain.R;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.JwtUtils;
import com.qf.im.entity.User;
import com.qf.im.service.UserService;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户表(User)表控制层
 *
 * @author makejava
 * @since 2023-07-26 18:50:59
 */
@RestController
@RequestMapping("user")
@Slf4j
@Validated
public class UserController  {
    /**
     * 服务对象
     */
    @Autowired
    private UserService userService;

    /**
     * 注册用户的接口
     * @param user
     * @return
     */
    @RequestMapping("/register")
    public R register(@Validated User user){
        System.out.println(user);
        log.debug("[user register] 用户注册-{}",user);
        userService.save(user);
        return R.succ();
    }

    /**
     * 用户登录的接口
     * @param username
     * @param password
     * @return
     */

    @RequestMapping("/login")
    public R login(@NotBlank(message = "用户名不能为空") String username, @jakarta.validation.constraints.NotBlank(message = "密码不能为空") String password){
        log.debug("[user login] 用户登录 - {} - {}", username, password);
        User user = userService.queryByUserName(username);
        if(user==null||!user.getPassword().equals(password)){
            throw new ServiceException(Codes.LOGIN_FAIL);
        }
        //将用户信息转换成jwt令牌
        String token = JwtUtils.createJwtToken()
                .add("uid", user.getId())
                .add("header", user.getHeader())
                .add("nickname", user.getNickname())
                .build();

        //登录成功
        return R.succ(token);
    }


    /**
     * 发送验证码
     * @param username
     * @return
     */
    @RequestMapping("/sendCode")
    public R sendCode(@NotBlank(message = "用户名不能为空") String username){
        log.debug("[send code]发送验证码--{}",username);
        userService.sendEmailCode(username);
        return R.succ();
    }

    /**
     * 修改密码
     * @param username
     * @param password
     * @param code
     * @return
     */
    @RequestMapping("/updatePassword")
    public R updatePassword(@NotBlank(message = "用户名不能为空") String username,
                            @NotBlank(message = "修改后的密码不能为空") String password,
                            @NotNull(message = "验证码不能为空") Integer code){
        log.debug("[update password]-修改密码--{}-{}--{}",username,password,code);
        userService.updatePassword(username,password,code);

        return R.succ();
    }

    /**
     * 搜索好友的方法
     * @return
     */
    @RequestMapping("/search")
    public R search(String keyword){
        log.debug("[search keyword]-查找好友--{}-",keyword);
        List<User> list = userService.searchByKeyword(keyword);
        return R.succ(list);
    }
    @RequestMapping("/query")
    public R query(Integer id){
        User user = userService.query().eq("id", id).one();
        log.debug("[query id]-查找好友--{}-",user);
        return R.succ(user);
    }

}

