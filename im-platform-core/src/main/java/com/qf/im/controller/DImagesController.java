package com.qf.im.controller;


import com.qf.im.service.DImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 动态图片名资源表(DImages)表控制层
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@RestController
@RequestMapping("dImages")

public class DImagesController {
    /**
     * 服务对象
     */
    @Autowired
    private DImagesService dImagesService;


}

