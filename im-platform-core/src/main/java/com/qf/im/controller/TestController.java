package com.qf.im.controller;

import com.qf.commons.aspect.anotation.GetUser;
import com.qf.commons.utils.AuthUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

//    private Logger logger= LoggerFactory.getLogger(TestController.class);
    @RequestMapping("/query")
    @GetUser
    public String test(){
        Integer uid = AuthUtils.get();
        log.debug("查询方法已经触发 - {}",uid);

        System.out.println(uid);
        return "succ";
    }
}
