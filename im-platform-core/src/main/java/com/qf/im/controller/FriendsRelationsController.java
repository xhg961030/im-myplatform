package com.qf.im.controller;


import com.qf.commons.aspect.anotation.GetUser;
import com.qf.commons.domain.R;
import com.qf.commons.utils.AuthUtils;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.vo.FriendsShowVo;
import com.qf.im.service.FriendsRelationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表控制层
 *
 * @author makejava
 * @since 2023-07-28 20:07:18
 */
@RestController
@RequestMapping("friendsRelations")
public class FriendsRelationsController {
    /**
     * 服务对象
     */
    @Autowired
    private FriendsRelationsService friendsRelationsService;
    @GetUser
    @RequestMapping("/friendsList")
    public R friendsList(){
        Integer uid = AuthUtils.get();

        List<FriendsShowVo> friends =
                friendsRelationsService.queryFriend(uid);

        return R.succ(friends);
    }
    //获取所有的uid
    @GetUser
    @RequestMapping("/fidList")
    public R fidList(){
        Integer uid = AuthUtils.get();
        System.out.println(uid);

        List<FriendsRelations> list = friendsRelationsService.query().eq("uid", uid).list();
        System.out.println(list);
        return R.succ(list);
    }
}

