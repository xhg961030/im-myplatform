package com.qf.im.controller;


import com.qf.commons.domain.R;
import com.qf.im.entity.vo.DongTaiHandler;
import com.qf.im.entity.vo.DongTaiVo;
import com.qf.im.service.DongTaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 动态表(DongTai)表控制层
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
@RestController
@RequestMapping("dongTai")
public class DongTaiController  {
    /**
     * 服务对象
     */
    @Autowired
    private DongTaiService dongTaiService;
    /**
     * 入参：用户id，文本内容 ，图片资源名称集合
     * @return
     */
    @RequestMapping("/addDongTai")
    public R addDongTai(DongTaiHandler dongTai){
        System.out.println(dongTai);
        int res = dongTaiService.addDongTai(dongTai);
        return R.succ();
    }

    /**
     * 2、动态查询展示列表(三表查询，要备注则需要四表查询）
     * 入参：无，令牌获取uid
     * 返回：
     * #查询动态id,用户昵称（包括自己），用户头像，动态内容，动态创建时间, 动态图片资源集合（返回一个动态vo实体，必须使用mapper 文件
     * SELECT   di.img_name , dt.id  , dt.d_content, dt.create_time ,u.nickname ,u.header
     * FROM USER u INNER JOIN dong_tai  dt ON dt.uid = u.id
     * INNER JOIN d_images di ON di.did = dt.id
     * and  u.id=#{uid}
     */
    @RequestMapping("/showDongTai")
    public R showDongTai(){
        List<DongTaiVo> dongTaiVoList = dongTaiService.showQueryDT();
        return R.succ(dongTaiVoList);
    }

}

