package com.qf.im.application;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
@EnableAspectJAutoProxy
@ComponentScan("com.qf")
@MapperScan("com.qf.im.dao")
@EnableAsync
@EnableTransactionManagement
public class ImApplication {
    //测试
    public static void main(String[] args) {
        SpringApplication.run(ImApplication.class, args);
    }

    /**
     * 统一解决跨域的过滤器
     * @return
     */
    @Bean
    public CorsFilter corsFilter(){
        //初始化跨域的配置对象
        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        corsConfiguration.setAllowCredentials(true);//是否运行cookie跨域
//        corsConfiguration.addAllowedOrigin("http://localhost:8080");//任意host来源
        corsConfiguration.setAllowCredentials(false);//是否运行cookie跨域
        corsConfiguration.addAllowedOrigin("*");//任意host来源
        corsConfiguration.addAllowedHeader("*");//任意请求头
        corsConfiguration.addAllowedMethod("*");//任意请求类型 POST/GET

        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        //设置跨域的配置
        corsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);

        //初始化跨域过滤器
        CorsFilter corsFilter = new CorsFilter(corsConfigurationSource);
        return corsFilter;
    }
}
