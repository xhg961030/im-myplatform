package com.qf.im.application;

import com.qf.im.websocket.WsServer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 应用的启动、销毁监听类
 */
@Component
public class ApplicationRunning implements CommandLineRunner, DisposableBean {

    @Autowired
    private WsServer wsServer;

    @Override
    public void run(String... args) throws Exception {
        wsServer.start();
    }

    @Override
    public void destroy() throws Exception {
        wsServer.destory();
    }
}
