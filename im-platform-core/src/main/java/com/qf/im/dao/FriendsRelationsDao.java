package com.qf.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.vo.FriendsShowVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表数据库访问层
 *
 * @author makejava
 * @since 2023-07-28 20:07:18
 */
@Mapper
public interface FriendsRelationsDao extends BaseMapper<FriendsRelations> {
    @Select("""
        SELECT u.id,fr.beizhu, u.header, u.nickname FROM friends_relations 
        fr INNER JOIN user u ON  u.id=fr.fid    WHERE (uid = #{uid})
        """)
    List<FriendsShowVo> queryFriend(Integer uid);
}

