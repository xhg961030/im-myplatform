package com.qf.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.im.entity.DongTai;
import com.qf.im.entity.vo.DongTaiVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 动态表(DongTai)表数据库访问层
 *
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
@Mapper
public interface DongTaiDao extends BaseMapper<DongTai> {
    //查询好友动态
    public List<DongTaiVo> showQueryDT(Integer uid);
    //查询个人动态
    List<DongTaiVo> showQuery(Integer uid);
}

