package com.qf.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.im.entity.Comments;
import com.qf.im.entity.vo.CommentsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 评论表(Comments)表数据库访问层
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@Mapper
public interface CommentsDao extends BaseMapper<Comments> {
    @Select("""
             SELECT c.did,c.id,c.cid,c.fid, c.c_content ,u.nickname,c.create_time,u.header
                  FROM comments c INNER JOIN USER u ON u.id =c.fid  AND c.did =#{did} order by 
                  c.create_time desc
            """)
    List<CommentsVo> queryComments(Integer did);
}

