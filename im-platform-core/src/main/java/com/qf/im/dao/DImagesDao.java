package com.qf.im.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.im.entity.DImages;
import org.apache.ibatis.annotations.Mapper;

/**
 * 动态图片名资源表(DImages)表数据库访问层
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@Mapper
public interface DImagesDao extends BaseMapper<DImages> {

}

