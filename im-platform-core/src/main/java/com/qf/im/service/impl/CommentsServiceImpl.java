package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.commons.aspect.anotation.GetUser;
import com.qf.commons.domain.Codes;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.AuthUtils;
import com.qf.im.dao.CommentsDao;
import com.qf.im.entity.Comments;
import com.qf.im.entity.vo.CommentsVo;
import com.qf.im.service.CommentsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 评论表(Comments)表服务实现类
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
@Service("commentsService")
@Slf4j
public class CommentsServiceImpl extends ServiceImpl<CommentsDao, Comments> implements CommentsService {

    /**
     * 添加一条评论
     * 所属评论的cid,第一次评论则为nul，所属动态的id，用户id (令牌获取，可以不传)，评论的内容
     * 评论的内容insert into comninents values(....)
     * @return
     */
    @Override
    @GetUser
    @Transactional
    public int addComment(Comments comments) {
        log.debug("[add comment]添加了一条评论-{comments}",comments);
        Integer uid = AuthUtils.get();
        //设置评论人的id
        comments.setFid(uid);
        boolean res = this.save(comments);
        if(!res){
            throw new ServiceException(Codes.FAIL);
        }
        return 1;
    }

    /**
     * 根据动态id查询相关动态评论
     * @param did
     * @return
     */
    @Override
    public List<CommentsVo> queryComments(Integer did) {
        return getBaseMapper().queryComments(did);
    }
}

