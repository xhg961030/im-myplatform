package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.commons.domain.Codes;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.CodesUtils;
import com.qf.im.dao.UserDao;
import com.qf.im.entity.Email;
import com.qf.im.entity.User;
import com.qf.im.service.EmailService;
import com.qf.im.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户表(User)表服务实现类
 *
 * @author makejava
 * @since 2023-07-26 18:50:59
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Override
    public boolean save(User entity) {
        //效验用户名是否存在
        Long userCount = this.query().eq("username", entity.getUsername()).count();
        if(userCount!=0){
            throw new ServiceException(Codes.USER_EXIST);
        }
        //校验邮箱是否存在
        Long emailCount = this.query().eq("email", entity.getEmail()).count();
        if( emailCount!=0){
            throw new ServiceException(Codes.EMAIL_EXIST);
        }

        return super.save(entity);
    }



    @Override
    public User queryByUserName(String username) {

        return this.query().eq("username",username).one();
    }



    @Autowired
    private EmailService emailService;
    @Override
    public int sendEmailCode(String username) {
        User user = this.queryByUserName(username);
        if(user==null){
            throw new ServiceException(Codes.USER_NOT_EXIST);
        }
        //获取email
        String sendEmail = user.getEmail();
//        System.out.println(sendEmail);
        //创建验证码
        int sendCode= (int) (Math.random()*9000+1000);
        //储存验证码五分钟有效期
        CodesUtils.putCode(username,sendCode,5, TimeUnit.MINUTES);
        //发送邮件
        Email email = new Email()
                .setContent("找回的验证码为："+sendCode+"如果不是本人操作，请忽略")
                .setSubject("官方邮件找回密码")
                .setTo(sendEmail);
        emailService.sendEmail(email);
        return 1;
    }

    @Override
    public int updatePassword(String username, String password, Integer code) {

        Integer sendCode = CodesUtils.getCode(username);
        if(sendCode==null||!sendCode.equals(code)){
            throw new ServiceException(Codes.CODE_ERROR);
        }
        CodesUtils.remove(username);
        this.update()
                .set("password", password)
                .eq("username", username)
                .update();
        return 1;
    }

    @Override
    public List<User> searchByKeyword(String keyword) {
        if(!StringUtils.hasLength(keyword)){
            return null;
        }
        //根据keyword模糊用户
        List<User> list = this.query()
                .select("id", "nickname", "sex", "header")
                .like("username", keyword)
                .or()
                .like("nickname", keyword)
                .list();
        return list;
    }
}

