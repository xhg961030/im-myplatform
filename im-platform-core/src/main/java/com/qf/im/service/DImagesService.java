package com.qf.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.im.entity.DImages;

/**
 * 动态图片名资源表(DImages)表服务接口
 *
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
public interface DImagesService extends IService<DImages> {

}

