package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.im.dao.DImagesDao;
import com.qf.im.entity.DImages;
import com.qf.im.service.DImagesService;
import org.springframework.stereotype.Service;

/**
 * 动态图片名资源表(DImages)表服务实现类
 *
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
@Service("dImagesService")
public class DImagesServiceImpl extends ServiceImpl<DImagesDao, DImages> implements DImagesService {

}

