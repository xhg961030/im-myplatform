package com.qf.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.im.entity.Comments;
import com.qf.im.entity.vo.CommentsVo;

import java.util.List;

/**
 * 评论表(Comments)表服务接口
 *
 * @author makejava
 * @since 2023-08-05 08:50:41
 */
public interface CommentsService extends IService<Comments> {

    int addComment(Comments comments);

    List<CommentsVo> queryComments(Integer did);
}

