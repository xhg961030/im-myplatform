package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.commons.aspect.anotation.GetUser;
import com.qf.commons.utils.AuthUtils;
import com.qf.im.dao.DImagesDao;
import com.qf.im.dao.DongTaiDao;
import com.qf.im.entity.DImages;
import com.qf.im.entity.DongTai;
import com.qf.im.entity.vo.DongTaiHandler;
import com.qf.im.entity.vo.DongTaiVo;
import com.qf.im.service.DongTaiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 动态表(DongTai)表服务实现类
 *
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
@Service("dongTaiService")
@Slf4j
public class DongTaiServiceImpl extends ServiceImpl<DongTaiDao, DongTai> implements DongTaiService {
    @Autowired
    private DImagesDao imagesDao;
    /**
     * 入参：用户id，文本内容 ，图片资源名称集合
     * 返回：
     * insert into dong_tai(   )  values( 用户id,文本内容  ) //获取动态id
     * insert into d-image()  values(动态id,图片名称)//将动态id和图片资源名批量插入
     * @param dongTai
     * @return
     */
    @Override
    @GetUser
    @Transactional
    public int addDongTai(DongTaiHandler dongTai) {
        //获取用户id
        Integer uid = AuthUtils.get();
        dongTai.setUid(uid);
        //添加动态记录 主键回填
        DongTai dongTaiE = new DongTai()
                .setUid(dongTai.getUid())
                .setDContent(dongTai.getDContent());
        boolean insert = this.save(dongTaiE);
        if(insert){
            //获取动态id
            Integer did = dongTaiE.getId();
            //循环插入每一条的图片记录
            System.out.println("图片资源"+dongTai.getImages());
            if(dongTai.getImages().size()!=0){
                for (String imgName : dongTai.getImages()) {
                    //设置动态图片资源的名称和动态id
                    DImages dImages = new DImages()
                            .setDid(did).setImgName(imgName);
                    //添加
                    int res = imagesDao.insert(dImages);
                    if(res==0){
                        System.out.println("添加图片记录失败");
                    }
                }
            }else {
                //添加一条为空的图片动态资源记录
                int res = imagesDao.insert(new DImages().setDid(did));
                System.out.println("添加一条为空的图片动态资源记录");
            }
        }else {
            System.out.println("添加动态失败");
        }
        return 1;
    }

    @Override
    @GetUser
    public List<DongTaiVo> showQueryDT() {
        //获取个人uid
        Integer uid = AuthUtils.get();
        //根据uid查找所有好友的动态内容
        List<DongTaiVo> dongTaiVoList = getBaseMapper().showQueryDT(uid);
        System.out.println("动态列表："+dongTaiVoList);
        return dongTaiVoList;
    }
}

