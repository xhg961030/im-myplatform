package com.qf.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.im.dao.FriendsRelationsDao;
import com.qf.im.entity.FriendsRelations;
import com.qf.im.entity.vo.FriendsShowVo;
import com.qf.im.service.FriendsRelationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 好友关系表(FriendsRelations)表服务实现类
 *
 * @author makejava
 * @since 2023-07-28 20:07:19
 */
@Service("friendsRelationsService")
public class FriendsRelationsServiceImpl extends ServiceImpl<FriendsRelationsDao, FriendsRelations> implements FriendsRelationsService {
    @Autowired
    private FriendsRelationsDao friendsRelationsDao;
    @Override
    public List<FriendsShowVo> queryFriend(Integer uid) {

        return friendsRelationsDao.queryFriend(uid);
    }
}

