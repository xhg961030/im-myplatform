package com.qf.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.im.entity.DongTai;
import com.qf.im.entity.vo.DongTaiHandler;
import com.qf.im.entity.vo.DongTaiVo;

import java.util.List;

/**
 * 动态表(DongTai)表服务接口
 *
 * @author makejava
 * @since 2023-08-05 08:50:42
 */
public interface DongTaiService extends IService<DongTai> {

    int addDongTai(DongTaiHandler dongTai);

    List<DongTaiVo> showQueryDT();
}

