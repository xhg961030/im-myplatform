package com.qf.commons.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class AsyncConfiguration  implements AsyncConfigurer {
    @Bean
    public ThreadPoolTaskExecutor getThreadPoolTaskExecutor(){
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        //获取核心数
        int core = Runtime.getRuntime().availableProcessors();
        threadPoolTaskExecutor.setCorePoolSize(core*3);
        threadPoolTaskExecutor.setMaxPoolSize(core*4);
        threadPoolTaskExecutor.setKeepAliveSeconds(60);
        threadPoolTaskExecutor.setQueueCapacity(10000);
        //定义自身的线程前缀名
        threadPoolTaskExecutor.setThreadNamePrefix("myThread-");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;

    }

    @Override
    public Executor getAsyncExecutor() {
        return getThreadPoolTaskExecutor();
    }
}
