package com.qf.commons.domain;

public enum Codes {

    SUCC(200, "成功"),
    FAIL(500, "服务器异常!"),
    NOT_POWER(501, "权限不足"),
    PARAMS_BIND(502, "参数校验失败"),
    USER_EXIST(503,"用户名已存着"),
    EMAIL_EXIST(504,"email已经存在"),
    FILE_NOT_FOUND(505, "文件未找到"),
    LOGIN_FAIL(506, "用户名或密码错误"),
    AUTH_ERROR(507,"用户认证失败" ),
    USER_NOT_EXIST(508,"用户名不存在" ),
    CODE_ERROR(509,"验证码过期，或者验证码不对" ),
    NOT_REQUEST_SELF(510,"不能申请自己为好友" ),
    REQUEST_EXIST(511,"申请已经存在，不能重复申请" ),
    FRIENDS_EXISTS(512,"已经是好友了" );

    private Integer code;
    private String msg;

    Codes(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
