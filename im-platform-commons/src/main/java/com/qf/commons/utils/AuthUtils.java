package com.qf.commons.utils;

/**
 * 存储用户id的工具类
 */
public class AuthUtils {
    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();
    public static void set(Integer uid){
        threadLocal.set(uid);
    }
    public static Integer get(){
        return threadLocal.get();
    }
    public static void clear(){
        threadLocal.remove();
    }
}
