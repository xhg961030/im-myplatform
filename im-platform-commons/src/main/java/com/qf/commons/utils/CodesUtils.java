package com.qf.commons.utils;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 验证码的工具类
 */
public class CodesUtils {
    @Data
    @Accessors(chain = true)
    private static class Val<T>{
        private T data;//数据
        private long ttl;
    }
    private static Map<String,Val> codeMap = new ConcurrentHashMap<>();

    public static void putCode(String key, Integer code, Integer ttl, TimeUnit timeUnit){
        Val val = new Val().setData(code)
                .setTtl(System.currentTimeMillis()+ timeUnit.toMillis(ttl));
        codeMap.put(key,val);
    }
    public static Integer getCode(String key){
        Val<Integer> val = codeMap.get(key);
        if(val.getTtl()<System.currentTimeMillis()){
            remove(key);
            return null;
        }
        return val.getData();
    }
    public static void remove(String key) {
        codeMap.remove(key);
    }
}
