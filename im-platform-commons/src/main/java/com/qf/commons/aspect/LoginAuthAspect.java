package com.qf.commons.aspect;

import com.qf.commons.domain.Codes;
import com.qf.commons.exception.ServiceException;
import com.qf.commons.utils.AuthUtils;
import com.qf.commons.utils.JwtUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
@Aspect
public class LoginAuthAspect {
    @Pointcut("@annotation(com.qf.commons.aspect.anotation.GetUser)")
    public void point(){

    }
    @Around("point()")
    public Object loginTokenParse(ProceedingJoinPoint joinPoint){
        //1、接收请求获取令牌（请求头）
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        String loginToken = request.getHeader("Login-Token");
        //2、解析
        Integer uid = JwtUtils.parseJwtToken(loginToken, "uid");

        if(uid==null){
            throw new ServiceException(Codes.AUTH_ERROR);
        }
        //3、存入令牌
        AuthUtils.set(uid);
        //4、放行
        try {
            //6、返回核心业务结果
            return  joinPoint.proceed();
        } catch (ServiceException e){
            throw e;
        }catch (Throwable e) {
            throw new RuntimeException(e);
        }finally {
            AuthUtils.clear();
        }


    }

}
