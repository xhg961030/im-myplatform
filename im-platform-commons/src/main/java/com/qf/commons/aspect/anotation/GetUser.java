package com.qf.commons.aspect.anotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME )
public @interface GetUser {
}
