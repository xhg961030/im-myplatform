create database im_db;

#用户表
create table user(
id int primary key auto_increment comment '主键',
username varchar(20) unique not null comment '用户名',
password varchar(20) not null comment '密码',
nickname varchar(10) not null comment '昵称',
header text comment '头像',
sex tinyint comment '性别 0-男 1-女',
email varchar(30) not null unique comment '邮箱',
create_time datetime not null comment '创建时间',
update_time datetime not null comment '最后修改时间',
status tinyint not null comment '状态',
del_flag tinyint not null comment '删除标识 0-正常 1-已经删除'
) comment '用户表';

#好友申请表
create table friends_request(
id int primary key auto_increment comment '主键',
uid int not null comment '申请者id',
tid int not null comment '被申请者id',
beizhu varchar(10) comment '好友备注',
info varchar(50) comment '申请信息',
create_time datetime not null comment '创建时间',
update_time datetime not null comment '最后修改时间',
status tinyint not null comment '状态(0-待处理 1-同意 2-拒绝)',
del_flag tinyint not null comment '删除标识 0-正常 1-已经删除'
) comment "好友申请表";

#好友关系表
create table friends_relations(
  uid int not null comment '用户id',
  fid int not null comment '好友id',
  beizhu varchar(10) comment '好友备注',
  create_time datetime not null comment '创建时间',
  update_time datetime not null comment '最后修改时间',
  status tinyint not null comment '状态(0-正常好友 1-黑名单)',
  del_flag tinyint not null comment '删除标识 0-正常 1-已经删除',
  primary key(uid, fid) #联合主键
) comment '好友关系表';


CREATE TABLE dong_tai (
  id INT PRIMARY KEY AUTO_INCREMENT COMMENT '主键',
  uid INT NOT NULL COMMENT '用户id',
  d_content VARCHAR(200) NOT NULL COMMENT '动态文本发布的内容',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '最后修改时间',
  STATUS TINYINT NOT NULL COMMENT '状态',
  del_flag TINYINT NOT NULL COMMENT '删除标识 0-正常 1-已经删除'
) COMMENT '动态表';
CREATE TABLE comments (
  id INT PRIMARY KEY AUTO_INCREMENT COMMENT '评论表主键',
  cid INT NOT NULL COMMENT '评论所评论的id',
  fid INT NOT NULL COMMENT '评论者id',
  did INT NOT NULL COMMENT '动态id',
  c_content VARCHAR(50) NOT NULL COMMENT '评论表内容',
  create_time DATETIME NOT NULL COMMENT '创建时间',
  update_time DATETIME NOT NULL COMMENT '最后修改时间',
  STATUS TINYINT NOT NULL COMMENT '状态',
  del_flag TINYINT NOT NULL COMMENT '删除标识 0-正常 1-已经删除'
) COMMENT '评论表';
CREATE TABLE dtb_images (
    id INT PRIMARY KEY AUTO_INCREMENT COMMENT '图片id',
    did INT NOT NULL COMMENT '动态id',
    img_name TEXT NOT NULL COMMENT '图片名字',
    create_time DATETIME NOT NULL COMMENT '创建时间',
    update_time DATETIME NOT NULL COMMENT '最后修改时间',
    STATUS TINYINT NOT NULL COMMENT '状态',
    del_flag TINYINT NOT NULL COMMENT '删除标识 0-正常 1-已经删除'
) COMMENT '动态图片名资源表';